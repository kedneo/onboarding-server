const express = require('express');
path = require('path');
port = 4201;

const app = express();

app.use(express.static('./dist/onboarding-portal'));

app.get('/*', (req, res) => {
  res.sendFile( path.join(__dirname , '/dist/onboarding-portal/index.html'));
});

app.listen(process.env.PORT || port, ()=>{
  console.log('servidor executado na porta '+ port);
});